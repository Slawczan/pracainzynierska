import React from "react"

function guideArrow(props) {
  return (
    <svg viewBox="0 0 40 28" {...props}>
      <path d="M31.2 9.8l2.3-5.1 5.9 12.4L26.2 21l2.3-5.1c-11.6-4.3-22.9-3.3-28 2.2.1-1.1.4-2.3.9-3.3 3.5-7.8 16.5-9.9 29.8-5z" />
    </svg>
  )
}

export default guideArrow
