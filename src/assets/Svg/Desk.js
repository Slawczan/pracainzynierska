import React from 'react'

const Desk = props => (
  <svg
    id="prefix__Warstwa_52"
    x={0}
    y={0}
    viewBox="0 0 640 480"
    xmlSpace="preserve"
  >
    <style>
      {
        '.mainprefix__st1{fill:#ff7956;stroke:#000;stroke-width:4;stroke-miterlimit:10}.mainprefix__st2{fill:#ffde55}.mainprefix__st2,.mainprefix__st3{stroke:#000;stroke-width:4;stroke-miterlimit:10}'
      }
    </style>
    <path
      d="M638 71v37.7H2V71h636z"
      fill="#ffc477"
      className="mainprefix__st5"
      stroke="#000"
      strokeWidth={4}
      strokeMiterlimit={10}
    />
    <path className="mainprefix__st1" d="M2 108.7h32V478H2V108.7z" />
    <path className="mainprefix__st2" d="M462 356.2h176V478H462V356.2z" />
    <path className="mainprefix__st1" d="M462 232.5h176v123.8H462V232.5z" />
    <path className="mainprefix__st2" d="M462 108.7h176v123.8H462V108.7z" />
    <path
      className="mainprefix__st3"
      d="M534 145.8h32v24.8h-32v-24.8zM534 269.6h32v24.8h-32v-24.8zM534 393.4h32v24.8h-32v-24.8z"
    />
    <g>
      <path
        d="M380.5 53.2c0 4.8-1.2 9-3.6 12.5-2.4 3.5-5.2 5.3-8.5 5.3h-96.8c-3.3 0-6.1-1.8-8.5-5.3-2.4-3.5-3.6-7.7-3.6-12.5 0-2.6 1-6.6 3-12.2 0 0 2.4-5.5 6-19.4 1.3-4.9 2.7-12 3-21.6h96.8c0 6.8 1 14 3 21.5s4 14.1 6.1 19.7c2.1 5.6 3.1 9.6 3.1 12z"
        stroke="#000"
        className="mainprefix__st4"
        strokeWidth={3}
        strokeMiterlimit={10}
      />
    </g>
  </svg>
)

export default Desk