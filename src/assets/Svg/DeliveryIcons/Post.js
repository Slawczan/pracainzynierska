import React from 'react'

const Post = (props) => {
    return (
        <svg
            id="prefixPost__Warstwa_1"
            x={0}
            y={0}
            viewBox="0 0 360 200"
            xmlSpace="preserve"
            {...props}
        >
            <style>
                {
                    ".prefixPost__st7,.prefixPost__st8{stroke-miterlimit:10}.prefixPost__st7{fill:#d39437;stroke:#000;stroke-width:2.5;stroke-linejoin:round}.prefixPost__st8{fill:none;stroke:#d52b1e;stroke-width:3}.prefixPost__st10{fill:#d52b1e}.prefixPost__st11{fill:#ccc}.prefixPost__st12{fill:#222}.prefixPost__st13{fill:#e6e6e6}"
                }
            </style>
            <path
                d="M359.5 150.1v.9c0 3.3-2.6 6-5.8 6H2.5c-3.2 0-5.8-2.7-5.8-6v-.9c0-3.3 2.6-6 5.8-6h35.8v-33.8H108v33.8h163.4V115H335v29.1h18.8c3.1.1 5.7 2.7 5.7 6z"
                fillRule="evenodd"
                clipRule="evenodd"
                fill="#393534"
            />
            <g
                stroke="#000"
                strokeWidth={8}
                strokeLinecap="round"
                strokeMiterlimit={10}
            >
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    fill="#ffe0bd"
                    d="M277.1 66l-9.5 15.3"
                />
                <path fill="none" d="M277.1 81.3l-7.1-3.8" />
            </g>
            <path
                d="M272.3 26.8h-51.7c-3.7 0-6.7 3-6.7 6.7v40.9c0 3.7 3 6.7 6.7 6.7h51.7c3.7 0 6.7-3 6.7-6.7V33.5c0-3.7-3-6.7-6.7-6.7z"
                fill="none"
                stroke="#000"
                strokeMiterlimit={10}
            />
            <path
                d="M354.9 101.1l-31.4-14.8c-1.9-.9-3.4-2.6-4-4.6L294.7 5.3c-1-3.2-4-5.3-7.3-5.3H7.3C3.1 0-.4 3.4-.4 7.7v129c0 4.2 3.4 7.7 7.7 7.7h29c2.6 0 4.9-1.3 6.4-3.4 5.7-8.5 15.4-14.1 26.4-14.1 12.4 0 23.2 7.1 28.4 17.5h-.1l3.6 9.4c1.1 3 4 5 7.2 5h157.5c3.2 0 6-1.9 7.1-4.9l4.3-11h0c5.5-9.5 15.8-15.9 27.6-15.9 10.5 0 19.8 5.1 25.6 12.9 1.4 1.9 4.2 4.6 6.7 4.6h13.6c4.2 0 8.9-5 8.9-9.2v-27.1c-.2-3.1-1.9-5.8-4.6-7.1zM279 74.4c0 3.7-3 6.7-6.7 6.7h-51.7c-3.7 0-6.7-3-6.7-6.7V33.5c0-3.7 3-6.7 6.7-6.7h51.7c3.7 0 6.7 3 6.7 6.7v40.9zm6.2 6.7V27.2h6.6c7 0 7.8 6.2 7.8 6.2L314 81.1h-28.8z"
                fill="#fff"
                stroke="#000"
                strokeMiterlimit={10}
            />
            <path
                d="M229.9 88.7v-3.5l-14.7.8s-1 .2-1 1 1.2.8 1.2.8l14.5.9z"
                stroke="#000"
                strokeWidth={0.5}
                strokeLinejoin="round"
                strokeMiterlimit={10}
            />
            <path
                d="M359.2 106.7s-3.1-1.2-3.1 1.9v18.2s3.1 3.9 3.1 1.6v-21.7z"
                fill="#fff"
                stroke="#000"
                strokeWidth={2}
                strokeLinejoin="round"
                strokeMiterlimit={10}
            />
            <circle className="prefixPost__st7" cx={353.1} cy={121.1} r={2.9} />
            <path
                className="prefixPost__st8"
                d="M282.9 30.4v94.9H279c-5.8 7-12.3 26.4-12.3 26.4h-57.5V29.2c0-3.5 2.8-6.3 6.3-6.3h59.9c4.1 0 7.5 3.4 7.5 7.5zM209.2 151.7h-99.7s-6.4-33.2-40.9-33.6-36.9 22.1-36.9 22.1"
            />
            <path className="prefixPost__st8" d="M32.5 139.1H5.8V14.9S5.4 9.6-.3 8.8" />
            <path
                d="M-.2 106.7s3.1-1.2 3.1 1.9v18.2s-3.1 3.9-3.1 1.6v-21.7z"
                fill="#c4000d"
                stroke="#000"
                strokeWidth={0.75}
                strokeLinejoin="round"
                strokeMiterlimit={10}
            />
            <circle className="prefixPost__st7" cx={6} cy={121.3} r={2.9} />
            <path
                className="prefixPost__st10"
                d="M12.3 37.8h178v37.6h-178V37.8m32.3 7c-1.2.6-2.4 1.1-3.5 1.8 1.1-.2 2.5 0 3.4.7.8.7 1 1.8.7 2.8-.6 1.9-1.9 3.5-3.4 4.7-2.1 1.7-4.6 3-7.3 3.5-.6 0-1.2.3-1.8.1-1.1-.1-2.2-.5-2.8-1.4-.5-.9-.3-2 .1-2.9-1.3 1.2-2.6 2.4-3.7 3.8-.5.6-.9 1.3-.9 2.1 0 .6.7.8 1.2.9.9.1 1.8 0 2.7-.1 3.5-.4 7 .8 10.2 2.5 1.8.9 3.5 2 5.3 2.8 2.1.9 4.4 1.5 6.7 1.4 3 .1 5.9-.9 8.3-2.5 3.9-2.5 6.8-6.3 8.9-10.4 0-.4.2-.9-.3-1.1-.4-.2-.9.1-1.1.5-1.9 2.9-4.4 5.6-7.4 7.3-2 1.1-4.3 1.8-6.6 1.9-2 .1-4.1-.6-5.5-2.1-1.5-1.7-2.1-4.1-1.8-6.3.2-2.2 1-4.4 2.3-6.2.7-1.2 1.6-2.5 1.6-4 0-.6-.7-.8-1.2-.8-1.5 0-2.8.5-4.1 1m106 3.7c-.2 0-.5.1-.4.3v13.3c0 .4.5.4.8.4.3 0 .8 0 .8-.4V48.8c-.2-.5-.8-.3-1.2-.3m10.9.3v13.3c0 .4.6.3.8.3.3 0 .8 0 .8-.4v-4.9c1.3 1.6 2.5 3.3 3.7 4.9.4.6 1.1.4 1.7.4.3 0 .4-.4.2-.6-1.2-1.6-2.5-3.3-3.8-4.9l3.3-3.3c.2-.2.4-.6 0-.7-.6-.1-1.4-.2-1.8.3-1.1 1.2-2.2 2.5-3.4 3.7v-8.2c-.2-.3-.6-.2-1-.2 0 0-.5-.1-.5.3m-86 1.3v12c0 .4.4.4.7.4.3 0 .8.1 1-.3v-4.7c1.5 0 3.1.1 4.5-.7 1.6-1 2.2-3.2 1.5-5-.4-1.1-1.4-1.9-2.6-2.1-1.4-.3-2.9-.1-4.3-.2-.4-.1-.8.1-.8.6m55.4 0v11.8c0 .2 0 .4.2.4.4.1.9.1 1.3 0 .2 0 .2-.2.2-.4v-4.5c1.7 0 3.6.1 5-1.1 1.2-1.1 1.5-2.8 1.1-4.3-.3-1.1-1.2-2-2.3-2.3-1.5-.4-3.1-.2-4.7-.3-.4 0-.9.3-.8.7m-92.2.1c-1.4.3-2.7 1-3.6 2.1-.4.5-.9 1.1-.9 1.8 0 .5.4.9.9 1 .9.2 1.9-.1 2.7-.5 1.3-.7 2.7-1.7 2.9-3.3.1-1.1-1.2-1.3-2-1.1m72.9.4v2.2c-.5 0-1-.1-1.4 0-.3.3-.3.9 0 1.3.5.1.9 0 1.4 0v5.5c0 .9.2 1.8.9 2.4.8.6 2 .6 2.9.3.6-.2.5-.9.4-1.3 0-.2-.3-.2-.4-.1-.5.2-1.2.3-1.7 0-.4-.4-.5-1-.5-1.5v-5.2h2.4c.3-.3.3-.9 0-1.3h-2.5v-2.2c-.1-.3-.5-.2-.8-.3-.2 0-.5 0-.7.2m-54.8.5c-1.9.2-3.6 1.5-4.3 3.2-.5 1-.5 2.3.2 3.2.6.8 1.5 1.2 2.5 1.2 2.5.1 5-1.7 5.5-4.1.1-.8.1-1.8-.5-2.4-.8-1-2.2-1.3-3.4-1.1m41.8 1.6c-1.1 0-2.3.4-3 1.3-.9 1.1-1.1 2.5-1.1 3.9 0 1.3.3 2.8 1.3 3.7 1.5 1.3 3.8 1.2 5.3.1.4-.2.5-.7.4-1.2 0-.2 0-.6-.3-.6-.5.2-.8.6-1.3.8-1 .5-2.3.4-3-.5-.6-.8-.7-1.9-.7-2.9 0-1.1.2-2.3 1.1-3 1-.7 2.4-.4 3.4.4.2.2.6.5.8.1.1-.5.2-1.1-.3-1.4-.7-.5-1.7-.7-2.6-.7m44.9 0c-1.2.1-2.4.7-3.1 1.7-.8 1.2-1 2.6-.9 4 .1 1.2.5 2.4 1.4 3.2 1 .8 2.3 1 3.5.9 1.1-.1 2.2-.6 3-1.5.9-1.1 1.1-2.5 1.1-3.9 0-1.3-.4-2.7-1.4-3.5-1-.8-2.4-1-3.6-.9m13 0c-.8.1-1.6.4-2.1 1-.9 1-.8 2.7.2 3.5.9.9 2.3.9 3.3 1.7.7.5.5 1.7-.3 2.1-1.1.5-2.4.2-3.3-.4-.2-.1-.5-.3-.6-.1-.1.5-.2 1.2.4 1.5 1.4.7 3.1.8 4.5.1 1.5-.7 1.9-2.8.9-4-1-1-2.5-1.1-3.6-1.9-.7-.5-.5-1.7.2-2 .9-.4 2-.2 2.9.3.2.1.4.3.5 0 .1-.4.2-.9-.1-1.2-.9-.6-2-.7-2.9-.6m14.3.8c-.5.2-.5.9-.3 1.3.1.2.4.1.6 0 .9-.5 1.8-.9 2.8-.8.6 0 1.2.3 1.5.8.3.6.3 1.3.3 2-1.4 0-3-.1-4.3.6-1 .5-1.6 1.6-1.4 2.7 0 1.2 1 2.1 2.1 2.3 1.4.4 2.9-.2 3.9-1.2.1.4-.2 1.2.4 1.2.3 0 .9.1 1-.2v-6c0-.8-.1-1.7-.6-2.4-.5-.6-1.2-.9-2-1-1.4-.2-2.9 0-4 .7m-83.1-.7c-.8.1-1.6.5-2.1 1-1.2 1.1-1.5 2.7-1.4 4.3 0 1.3.4 2.6 1.3 3.5 1 .9 2.4 1.1 3.7 1 1.2-.1 2.3-.7 3-1.7.8-1.2 1-2.7.9-4.1-.1-1.2-.5-2.5-1.5-3.2-1.1-.9-2.6-1-3.9-.8m29.8.9c-.4.3-.3 1.6.3 1.2.9-.5 1.8-.9 2.9-.8.6 0 1.3.2 1.6.8.3.6.3 1.3.3 2-1.2 0-2.5-.1-3.7.3-.7.2-1.5.7-1.8 1.4-.5 1.2-.2 2.8.9 3.5 1.5.9 3.6.5 4.8-.8.1.4-.2 1.1.3 1.2.3 0 .9.1 1.1-.2v-5.8c0-.8 0-1.7-.5-2.4-.5-.8-1.5-1.2-2.4-1.3-1.3-.1-2.7.1-3.8.9m-14.2-.8c-.3.3-.3 1 0 1.3 1.3.1 2.6 0 4 0-1.4 2.1-2.7 4.2-4.1 6.3-.3.4-.3 1-.2 1.5 0 .2.3.4.5.3h5.3c.8.2.8-1.6 0-1.4h-4c1.3-2.1 2.7-4.2 4-6.3.3-.5.3-1 .2-1.5-.1-.3-.4-.3-.6-.3-1.8.1-3.5 0-5.1.1z"
            />
            <path
                className="prefixPost__st10"
                d="M77.2 50.9c1.2.1 2.7-.3 3.7.7s.9 3-.2 3.9c-1 .8-2.4.6-3.6.6.1-1.8.1-3.5.1-5.2zM132.6 50.9c1 0 2-.1 3 .2 1.9.7 1.9 3.7.2 4.6-1 .5-2.1.3-3.2.4v-5.2zM88.1 54.1c.7-.1 1.6 0 2.2.5.7.5.9 1.4 1 2.2.1 1.2.1 2.5-.6 3.5s-2.1 1.2-3.2.8c-.9-.3-1.4-1.2-1.6-2.1-.2-1.2-.2-2.4.3-3.5.4-.8 1.2-1.3 1.9-1.4zM143.6 54.1c.7-.1 1.5 0 2.1.5.7.5.9 1.4 1 2.2.1 1.1.1 2.3-.5 3.2-.9 1.6-3.6 1.7-4.5 0-.5-.8-.5-1.7-.5-2.6 0-1.4.8-3.2 2.4-3.3zM119.2 58.5c1-.6 2.3-.4 3.4-.4V60c-.7 1-2 1.7-3.2 1.2-1.1-.4-1.2-2.1-.2-2.7zM172.4 58.4c1-.6 2.2-.4 3.3-.4v1.9c-.5.5-1 1-1.7 1.2-.7.2-1.5.1-2-.4-.6-.6-.5-1.8.4-2.3z"
            />
            <g>
                <path d="M275.1 158.9c0 16.1 13 29.1 29.1 29.1s29.1-13 29.1-29.1h-58.2z" />
                <path
                    className="prefixPost__st11"
                    d="M325.6 158.9c0 11.8-9.6 21.3-21.3 21.3-11.8 0-21.3-9.6-21.3-21.3h42.6z"
                />
                <path d="M312.4 158.9c0 4.5-3.6 8.1-8.1 8.1s-8.1-3.6-8.1-8.1h16.2z" />
            </g>
            <g>
                <path
                    className="prefixPost__st12"
                    d="M333.4 158.9h-7.8c0-11.8-9.6-21.3-21.3-21.3-11.8 0-21.3 9.6-21.3 21.3h-7.8c0-16.1 13-29.1 29.1-29.1 16 0 29.1 13 29.1 29.1z"
                />
                <path
                    className="prefixPost__st13"
                    d="M325.6 158.9h-13.2c0-4.5-3.6-8.1-8.1-8.1s-8.1 3.6-8.1 8.1H283c0-11.8 9.6-21.3 21.3-21.3 11.7 0 21.3 9.5 21.3 21.3z"
                />
                <path
                    className="prefixPost__st12"
                    d="M312.4 158.9h-16.3c0-4.5 3.6-8.1 8.1-8.1 4.6-.1 8.2 3.6 8.2 8.1z"
                />
            </g>
            <g>
                <path d="M39.2 158.9c0 16.1 13 29.1 29.1 29.1s29.1-13 29.1-29.1H39.2z" />
                <path
                    className="prefixPost__st11"
                    d="M89.7 158.9c0 11.8-9.6 21.3-21.3 21.3S47 170.7 47 158.9h42.7z"
                />
                <path d="M76.5 158.9c0 4.5-3.6 8.1-8.1 8.1s-8.1-3.6-8.1-8.1h16.2z" />
            </g>
            <g>
                <path
                    className="prefixPost__st12"
                    d="M97.4 158.9h-7.8c0-11.8-9.6-21.3-21.3-21.3S47 147.1 47 158.9h-7.8c0-16.1 13-29.1 29.1-29.1s29.1 13 29.1 29.1z"
                />
                <path
                    className="prefixPost__st13"
                    d="M89.7 158.9H76.5c0-4.5-3.6-8.1-8.1-8.1s-8.1 3.6-8.1 8.1H47c0-11.8 9.6-21.3 21.3-21.3s21.4 9.5 21.4 21.3z"
                />
                <path
                    className="prefixPost__st12"
                    d="M76.5 158.9H60.2c0-4.5 3.6-8.1 8.1-8.1 4.5-.1 8.2 3.6 8.2 8.1z"
                />
            </g>
        </svg>
    )
}

export default Post
