import React from 'react'
import './WarehouseCheck.scss'

import OpinionContainer from '../UI/OpinionContainer/OpinionContainer'

const warehouseCheck = (props) => {

    let isInWarehouse = ["Książka", "Płyta", "Gazeta", "Gra planszowa"]
    if (props.isNotAvailable === true) {
        isInWarehouse = []
    }

    let show = null;
    show = props.choosenItems.map((item, index) => {
        if (isInWarehouse.includes(item.name)) {
            return (
                <OpinionContainer key={"Check" + index} text={item.name} type={'advantage'} />
            )
        } else {
            return (
                <OpinionContainer key={"Check" + index} text={item.name} type={'disadvantage'} />
            )
        }
    })

    return (
        <div className="warehouseCheckContainer">
            <div className="warehouseCheck">
                <h2 className="warehouseCheck__title">Dostępność zamówienia w magazynie:</h2>
                {show}
            </div>
        </div>
    )
}

export default warehouseCheck
