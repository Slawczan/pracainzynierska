import React, { useState } from 'react'
import './DeliveryProcess.scss'

import Lottie from 'react-lottie'
import animationDataInpost from './Animation/DeliveryProcessInpost.json'
import animationDataPost from './Animation/DeliveryProcessPost.json'
import animationDataDHL from './Animation/DeliveryProcessDHL.json'
import { Row } from 'react-bootstrap'

import NavigationPanel from '../UI/NavigationPanel/NavigationPanel'

const DeliveryProcess = (props) => {
    const [isStopped, setStop] = useState(false)
    const [isPaused, setPause] = useState(false)

    let choosenAnimation = null;

    if (props.choosenDeliveryMethod === "Poczta") {
        choosenAnimation = animationDataPost
    } else if (props.choosenDeliveryMethod === 'Paczkomat') {
        choosenAnimation = animationDataInpost
    } else if (props.choosenDeliveryMethod === 'Kurier(DHL)') {
        choosenAnimation = animationDataDHL
    }

    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: choosenAnimation,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };
    return (
        <div className="deliveryProcessContainer">
            <div className="deliveryProcess">
                <Row className="deliveryProcess__animation">
                    <Lottie
                        options={defaultOptions}
                        isStopped={isStopped}
                        isPaused={isPaused}
                    />
                </Row>
                <NavigationPanel
                    {...props}
                    setStopButton={() => setStop(true)}
                    setPlayButton={() => [setStop(false), setPause(false)]}
                    setPauseButton={() => setPause(!isPaused)}
                />
            </div>
        </div>
    )
}

export default DeliveryProcess
