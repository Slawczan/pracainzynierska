import React from 'react'
import './WelcomeBox.scss'
import { Button } from 'react-bootstrap'

import DeliveryMan from '../../assets/Svg/WelcomeBoxIcons/DeliveryMan'

const welcomeBox = (props) => {
    return (
        <div className="welcomeBox">
            <div className="welcomeBox__content">
                <h2 className="welcomeBox__content__title">Witaj w symulacji procesu</h2>
                <h4 className="welcomeBox__content__subtitle">Tematem przygotowanej symulacji jest proces dostarczenia zamówienia wybranego przez klienta w sklepie internetowym</h4>
                <h5 className="welcomeBox__content__instructions">Pierwszym etapem będzie wcielenie się w klienta i wybranie odpowiedniego zamówienia oraz sposobu dostawy</h5>
                <Button onClick={props.deleteModal} onKeyPress={props.deleteModal} className="welcomeBox__content__button">Zaczynajmy</Button>
            </div>
            <div className="welcomeBox__icon">
                <DeliveryMan className="welcomeBox__deliveryMan" />
            </div>
        </div>
    )
}

export default welcomeBox
