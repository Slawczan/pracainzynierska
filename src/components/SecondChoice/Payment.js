import React from 'react'
import './Payment.scss'

import Payment from '../../assets/Svg/Payment'

const payment = (props) => {

    return (
        <div className="paymentContainer">
            <div className="payment">
                <div className="payment__image">
                    <Payment />
                </div>
            </div>
        </div>
    )
}

export default payment