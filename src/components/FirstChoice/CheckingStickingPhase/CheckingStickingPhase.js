import React, { useState } from 'react'
import './CheckingStickingPhase.scss'

import Lottie from 'react-lottie'
import animationData from './Animation/CheckingStickingPhase.json'

import { Row } from 'react-bootstrap'

import NavigationPanel from '../../UI/NavigationPanel/NavigationPanel'

const CheckingStickingPhase = (props) => {
    const [isStopped, setStop] = useState(false)
    const [isPaused, setPause] = useState(false)

    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };

    return (
        <div className="checkingStickingPhaseAnimationContainer">
            <div className="checkingStickingPhaseAnimation">
                <Row className="checkingStickingPhaseAnimation__animation">
                    <Lottie
                        options={defaultOptions}
                        isStopped={isStopped}
                        isPaused={isPaused}
                    />
                </Row>
                <NavigationPanel
                    setStopButton={() => setStop(true)}
                    setPlayButton={() => [setStop(false), setPause(false)]}
                    setPauseButton={() => setPause(!isPaused)}
                />
            </div>
        </div>
    )
}

export default CheckingStickingPhase
