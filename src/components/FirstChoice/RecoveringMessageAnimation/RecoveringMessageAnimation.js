import React from 'react'
import './RecoveringMessageAnimation.scss'

import { Row } from 'react-bootstrap'

import Desk from '../../../assets/Svg/Desk'
import ShoppingItem from '../../ShoppingItem/ShoppingItem'

const recoveringMessageAnimation = (props) => {

    let show = props.choosenItems.map((item, index) => {
        return (
            <ShoppingItem key={index} name={item.name} md={3} clicked={null} disable={true} />
        )
    })
    return (
        <div className="deskAnimation">
            <div className="desk">
                <Row className="desk__head">
                    <h4 className="desk__title">Przedmioty zlecone do znalezienia:</h4>
                    {show}
                </Row>
                <Row className="desk__body">
                    <div className="desk__image">
                        <Desk />
                    </div>
                </Row>
            </div>
        </div>
    )
}

export default recoveringMessageAnimation
