import React from 'react'
import './Scaner.scss'

import Scaner from '../../../assets/Svg/Scaner'

const scaner = (props) => {

    return (
        <div className="scanerContainer">
            <div className="scaner">
                <div className="scaner__image">
                    <Scaner />
                </div>
            </div>
        </div>
    )
}

export default scaner
