import React, { useEffect } from 'react'
import './SpecificInfo.scss'

import { Container, Col, Row, Button } from 'react-bootstrap'

import SingleEntryPoint from '../../UI/GuideBox/SingleEntryPoint/SingleEntryPoint'
import OpinionContainer from '../../UI/OpinionContainer/OpinionContainer'

const SpecificInfo = (props) => {

    useEffect(() => {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });

    let text = null
    if (props.phase === props.totalAmountPhase - 1) {
        text = "Wróć do podsumowania"
    } else {
        text = "Przejdź do symulacji"
    }

    //Display map guide
    let showPoint = []
    for (let i = 0; i < props.totalAmountPhase - 1; i++) {
        showPoint.push(
            <SingleEntryPoint
                number={i + 1}
                lastPhase={props.totalAmountPhase}
                key={"EntryPoint" + i}
                phase={props.phase}
                changePhase={props.changePhase}
                specificInfoOn={true}
                choiceProcessNumber={props.choiceProcessNumber}
            />
        )
    }

    //Display general info
    let stageProcess = null
    stageProcess = props.processStages.map((item, index) => {
        return (
            <div className="specificInfo__stageDescription__singleStage" key={"ProcessDescription" + item.title}>
                <h4 className="specificInfo__stageDescription__singleStage__title">
                    {index + 1}. {item.title}
                </h4>
                <h6 className="specificInfo__stageDescription__singleStage__description">
                    {item.description}
                </h6>
            </div>
        )
    })

    //Advantage and disadvantage
    let advantage = props.advantages.map((item, index) => {
        return (
            <OpinionContainer key={"advantage" + index} type="advantage" text={item} />
        )
    })

    let disadvantage = props.disadvantages.map((item, index) => {
        return (
            <OpinionContainer key={"disadvantage" + index} type="disadvantage" text={item} />
        )
    })

    //Display left table
    let processStages = [...props.processStages]
    let cutIndex = null;
    processStages.forEach((item, index) => {
        if (item.title === "Poszukiwanie przedmiotu w magazynie") {
            cutIndex = index;
        }
    })
    if (cutIndex !== null) {
        processStages.splice(cutIndex, 3);
    }
    props.choosenItems.forEach(element => {
        if (element.name === "Książka") {
            processStages.splice(cutIndex, 0, {
                title: "Poszukiwanie książki w magazynie",
                place: "Główny magazyn",
                time: props.findTime
            })
        } else if (element.name === "Płyta") {
            processStages.splice(cutIndex, 0, {
                title: "Poszukiwanie płyty w magazynie",
                place: "Główny magazyn",
                time: props.findTime
            })
        } else if (element.name === "Gazeta") {
            processStages.splice(cutIndex, 0, {
                title: "Poszukiwanie gazety w magazynie",
                place: "Główny magazyn",
                time: props.findTime
            })
        } else if (element.name === "Gra planszowa") {
            processStages.splice(cutIndex, 0, {
                title: "Poszukiwanie gry planszowej w magazynie",
                place: "Główny magazyn",
                time: props.findTime
            })
        }
    })

    let tableShow = null
    tableShow = processStages.map((item, index) => {
        let showTime = null;
        let m = null;
        let h = null;
        let s = null;
        if (item.time < 60) {
            showTime = item.time + 's'
        } else if (item.time >= 60 && item.time < 3600) {
            m = Math.floor(item.time / 60)
            s = item.time - m * 60;
            if (s > 0) {
                showTime = m + 'min ' + s + 's'
            } else {
                showTime = m + 'min'
            }
        } else if (item.time >= 3600) {
            h = Math.floor(item.time / 3600);
            showTime = h + "h"
        }
        return (
            <tr className="specificInfo__table__body" key={"table" + item.title}>
                <th className="specificInfo__table__body__item">{index + 1}</th><td className="specificInfo__table__body__item">{item.title}</td><td className="specificInfo__table__body__item">{item.place}</td><td className="specificInfo__table__body__item">{showTime}</td>
            </tr>
        )
    })

    //Display right table
    let showTable2 = null
    let allStations = []

    processStages.forEach((item, index) => {
        if (index === 0) {
            allStations.push(item.place)
        } else {
            let validate = true;
            allStations.forEach(check => {
                if (check === item.place) {
                    validate = false;
                }
            })
            if (validate === true) {
                allStations.push(item.place)
            }
        }
    })

    showTable2 = allStations.map((item, index) => {
        let indexTable = ""
        let countTime = 0
        processStages.forEach((x, i) => {
            if (x.place === item) {
                indexTable = indexTable + (i + 1) + ','
                countTime += x.time
            }
        })
        indexTable = indexTable.slice(0, -1)
        let showTime = null;
        let m = null;
        let h = null;
        let s = null;
        let time = countTime
        if (time < 60) {
            showTime = time + 's'
        } else if (time >= 60 && time < 3600) {
            m = Math.floor(time / 60)
            s = time - m * 60;
            if (s > 0) {
                showTime = m + 'min ' + s + 's'
            } else {
                showTime = m + 'min'
            }
        } else if (time >= 3600) {
            h = Math.floor(time / 3600);
            showTime = h + "h"
        }

        return (
            <tr className="specificInfo__table__body" key={"table2" + index}>
                <th className="specificInfo__table__body__item">{index + 1}</th><td className="specificInfo__table__body__item">{item}</td><td className="specificInfo__table__body__item">{indexTable}</td><td className="specificInfo__table__body__item">{showTime}</td>
            </tr>
        )
    })

    return (
        <div className="specificInfo">
            <Row className="specificInfo__title">
                <h2>Nazwa wybranej ścieżki symulacji: <b>{props.title}</b></h2>
            </Row>

            <Row className="specificInfo__description">
                <h2>Opis szczegółowy symulacji:</h2>
                <h6>{props.processDescription}</h6>
            </Row>

            <Row className="specificInfo__guideDescription">
                <h2>Schemat poglądowy procesu</h2>
                <h6 className="specificInfo__guideDescription__tip">Kliknięcie poszczególnej ikony pozwoli na przeniesienie w konkretne miejsce procesu dostarczenia paczki.</h6>
                <h6 className="specificInfo__guideDescription__tip">Biała otoczka wskazuje na miejsce procesu w którym się znajdujesz.</h6>
            </Row>

            <Row className="specificInfo__guide">
                {showPoint}
            </Row>

            <Row className="specificInfo__stageDescription">
                <h2 className="specificInfo__stageDescription__title">Opis poszczególnych etapów procesu:</h2>
                {stageProcess}
            </Row>

            <Row className="specificInfo__section">
                <h3 className="specificInfo__section__text">Zalety i wady sposoby składowania przedmiotów w wybranej symulacji</h3>
                <Col md={6}>
                    <Container className="specificInfo__section__opinion">
                        {advantage}
                    </Container>
                </Col>
                <Col md={6}>
                    <Container className="specificInfo__section__opinion">
                        {disadvantage}
                    </Container>
                </Col>
            </Row>

            <Row className="specificInfo__tableWrapper">
                <Col md={6}>
                    <h2>Marszruta produkcyjna</h2>
                    <table className="specificInfo__table">
                        <thead>
                            <tr className="specificInfo__table__heading" >
                                <th className="specificInfo__table__heading__item">id</th>
                                <th className="specificInfo__table__heading__item">Nazwa operacji</th>
                                <th className="specificInfo__table__heading__item">Miejsce wykonywania operacji</th>
                                <th className="specificInfo__table__heading__item">Czas operacji</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tableShow}
                        </tbody>
                    </table>
                </Col>
                <Col md={6}>
                    <h2>Wykaz miejsc produkcyjnych</h2>
                    <table className="specificInfo__table">
                        <thead>
                            <tr className="specificInfo__table__heading" >
                                <th className="specificInfo__table__heading__item">id</th>
                                <th className="specificInfo__table__heading__item">Nazwa miejsca</th>
                                <th className="specificInfo__table__heading__item">Numery id operacji</th>
                                <th className="specificInfo__table__heading__item">Całkowity czas operacji</th>
                            </tr>
                        </thead>
                        <tbody>
                            {showTable2}
                        </tbody>
                    </table>
                </Col>
            </Row>

            <Row>
                <Button className="specificInfo__button" onClick={props.forceInfo} onKeyPress={props.forceInfo}>
                    {text}
                </Button>
            </Row>

        </div>
    )
}

export default SpecificInfo
