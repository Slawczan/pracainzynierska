import React, { useState } from 'react'
import './MovingToWarehouse.scss'

import Lottie from 'react-lottie'
import animationData from './Animation/MovingToWarehouse2.json'

import { Row } from 'react-bootstrap'

import NavigationPanel from '../../UI/NavigationPanel/NavigationPanel'

const MovingToWarehouse = (props) => {
    const [isStopped, setStop] = useState(false)
    const [isPaused, setPause] = useState(false)

    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };
    return (
        <div className="movingToWarehouseContainer">
            <div className="movingToWarehouse">
                <Row className="movingToWarehouse__animation">
                    <Lottie
                        options={defaultOptions}
                        isStopped={isStopped}
                        isPaused={isPaused}
                    />
                </Row>
                <NavigationPanel
                    {...props}
                    setStopButton={() => setStop(true)}
                    setPlayButton={() => [setStop(false), setPause(false)]}
                    setPauseButton={() => setPause(!isPaused)}
                />
            </div>
        </div>
    )
}

export default MovingToWarehouse
