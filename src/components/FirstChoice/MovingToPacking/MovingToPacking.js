import React, { useState } from 'react'
import './MovingToPacking.scss'

import Lottie from 'react-lottie'
import animationData from './Animation/MovingToPacking.json'

import { Row } from 'react-bootstrap'

import NavigationPanel from '../../UI/NavigationPanel/NavigationPanel'

const MovingToPacking = (props) => {
    const [isStopped, setStop] = useState(false)
    const [isPaused, setPause] = useState(false)

    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };
    return (
        <div className="movingToPackingContainer">
            <div className="movingToPacking">
                <Row className="movingToPacking__animation">
                    <Lottie
                        options={defaultOptions}
                        isStopped={isStopped}
                        isPaused={isPaused}
                    />
                </Row>
                <NavigationPanel
                    {...props}
                    setStopButton={() => setStop(true)}
                    setPlayButton={() => [setStop(false), setPause(false)]}
                    setPauseButton={() => setPause(!isPaused)}
                />
            </div>
        </div>
    )
}

export default MovingToPacking
