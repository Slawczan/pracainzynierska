import React from 'react'
import './ChoicesPanel.scss'
import { Container, Row, Col } from 'react-bootstrap'

import SinglePanel from './SinglePanel/SinglePanel'

const choicesPanel = (props) => {
    const title = ["Magazyn", "Brak towaru", "Inne podejście"]
    const description = [
        "Scenariusz zakłada podstawową symulację procesu dostarczenia zamówionego towaru do klienta",
        "Scenariusz zakłada symulację procesu dostarczenia produktu gdy jednego bądź kilku przedmiotów nie ma w magazynie",
        "Scenariusz zakłada symulację procesu w którym zaprezentowane jest inne podejście do prowadzenia magazynu o nazwie cross-docking"
    ];

    return (
        <Container>
            <Row className="choicesPanel">
                <Col md={4}>
                    <SinglePanel {...props} number="0" title={title[0]} description={description[0]} />
                </Col>
                <Col md={4}>
                    <SinglePanel {...props} number="1" title={title[1]} description={description[1]} />
                </Col>
                <Col md={4}>
                    <SinglePanel {...props} number="2" title={title[2]} description={description[2]} />
                </Col>
            </Row>
        </Container>

    )
}

export default choicesPanel
