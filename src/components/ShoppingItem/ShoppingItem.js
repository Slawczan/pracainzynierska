import React from 'react'
import './ShoppingItem.scss'
import classNames from 'classnames'

import { Col } from 'react-bootstrap'

import Book from '../../assets/Svg/ShoppingIcons/Book'
import Checkers from '../../assets/Svg/ShoppingIcons/Checkers'
import Plate from '../../assets/Svg/ShoppingIcons/Plate'
import NewsPaper from '../../assets/Svg/ShoppingIcons/NewsPaper'

const shoppingItem = (props) => {

    const classValue = classNames({
        'item': true,
        'item--active': props.selected,
        'item--disable': props.disable
    })

    let svgShow = null;
    if (props.name === "Książka") {
        svgShow = <Book />
    } else if (props.name === "Płyta") {
        svgShow = <Plate />
    } else if (props.name === "Gazeta") {
        svgShow = <NewsPaper />
    } else if (props.name === "Gra planszowa") {
        svgShow = <Checkers />
    }

    let tabIndexWork = 0;
    if (props.disable === true) {
        tabIndexWork = -1;
    }

    return (
        <Col md={props.md}>
            <div onClick={props.clicked} onKeyPress={props.clicked} className={classValue} tabIndex={tabIndexWork} >
                <div className="item__image">
                    {svgShow}
                </div>
                <h2 className="item__title">{props.name}</h2>
            </div>
        </Col>
    )
}

export default shoppingItem
