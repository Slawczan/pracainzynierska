import React from 'react'
import './DeliveryItem.scss'
import { Col } from 'react-bootstrap'
import classNames from 'classnames'

import DHLIcon from '../../assets/Svg/DeliveryIcons/DHL'
import InpostIcon from '../../assets/Svg/DeliveryIcons/Inpost'
import PostIcon from '../../assets/Svg/DeliveryIcons/Post'

const deliveryItem = (props) => {

    let svgShow = null;
    if (props.name === "Kurier(DHL)") {
        svgShow = <DHLIcon />
    } else if (props.name === "Paczkomat") {
        svgShow = <InpostIcon />
    } else if (props.name === "Poczta") {
        svgShow = <PostIcon />
    }

    const classValue = classNames({
        'item': true,
        'item--active': props.selected,
        'item--disable': props.disable
    })

    let tabIndexWork = 0;
    if (props.disable === true) {
        tabIndexWork = -1;
    }

    return (
        <Col md={4}>
            <div onClick={props.clicked} onKeyPress={props.clicked} className={classValue} tabIndex={tabIndexWork} >
                <div className="deliveryItem__image">
                    {svgShow}
                </div>
                <h2 className="devlieryItem__title">{props.name}</h2>
            </div>
        </Col>
    )
}

export default deliveryItem
