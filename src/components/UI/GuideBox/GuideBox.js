import React from 'react'
import './GuideBox.scss'

import { Transition, animated } from 'react-spring/renderprops'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMap, faInfoCircle } from '@fortawesome/free-solid-svg-icons'

import SingleEntryPoint from './SingleEntryPoint/SingleEntryPoint'

const guideBox = (props) => {

    let show = null
    let icon = null

    if (props.phase === props.totalAmountPhase - 1) {
        return null
    }

    let showPoint = []

    for (let i = 0; i < props.totalAmountPhase - 1; i++) {
        showPoint.push(
            <SingleEntryPoint
                number={i + 1}
                lastPhase={props.totalAmountPhase}
                key={"EntryPoint" + i}
                phase={props.phase}
                changePhase={props.changePhase}
                choiceProcessNumber={props.choiceProcessNumber}
            />
        )
    }

    if (props.isOpen) {
        show = (
            <div className="guideBox" onClick={props.guideBoxClicked}>
                <div className="guideBox__header">
                    <h3 className="guideBox__title">
                        Stan procesu
                </h3>
                    <FontAwesomeIcon className="guideBox__infoIcon" icon={faInfoCircle} onClick={props.forceInfo} />
                </div>
                <div className="guideBox__content">
                    {showPoint}
                </div>
            </div>
        )
    } else {
        icon = (
            <FontAwesomeIcon className="guideBox__icon" icon={faMap} />
        )
    }
    return (
        <React.Fragment>
            <button className="guideBox__button" onClick={props.guideBoxClicked}>
                {icon}
            </button>
            <Transition
                config={{ duration: 750 }}
                items={show}
                from={{
                    position: 'fixed',
                    zIndex: 2,
                    bottom: 0,
                    right: 0,
                    transform: 'translate3d(-600px,0,0)',
                    opacity: 1
                }}
                enter={{
                    position: 'fixed',
                    bottom: 0,
                    left: 0,
                    zIndex: 2,
                    transform: 'translate3d(0,0,0)',
                    opacity: 1
                }}
                leave={{
                    position: 'fixed',
                    zIndex: 2,
                    bottom: 0,
                    left: 0,
                    transform: 'translate3d(-600px,0,0)',
                    opacity: 1
                }}>
                {(show) => show && (props => <animated.div className="InfoAnimation" style={props}>{show}</animated.div>)}
            </Transition>
        </React.Fragment >
    )
}

export default guideBox
