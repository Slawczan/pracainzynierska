import React from 'react'
import './SingleEntryPoint.scss'

import classNames from 'classnames'

import GuideIcons from '../../../../assets/Svg/GuideIcons/GuideIcons'
import GuideIconsSecond from '../../../../assets/Svg/GuideIcons/GuideIconsSecond'
import GuideIconsThird from '../../../../assets/Svg/GuideIcons/GuideIconsThird'
import GuideArrow from '../../../../assets/Svg/GuideIcons/GuideArrow'

const singleEntryPoint = (props) => {

    let showArrow = <GuideArrow />
    if (props.number === props.lastPhase - 1) showArrow = null

    const classValue = classNames({
        'entryPoint__process': true,
        'entryPoint__process--active': props.number - 1 === props.phase
    })
    const classValueNumber = classNames({
        'entryPoint__process__number': true,
        'entryPoint__process__number--active': props.number - 1 === props.phase
    })
    const entryPoint__wrapper = classNames({
        'entryPoint__wrapper': true,
        'entryPoint__wrapper--specificInfo': props.specificInfoOn === true
    })

    let whichGuideIcons = null
    if (props.choiceProcessNumber === 1) {
        whichGuideIcons = <GuideIcons number={props.number - 1} />
    } else if (props.choiceProcessNumber === 2) {
        whichGuideIcons = <GuideIconsSecond number={props.number - 1} />
    } else if (props.choiceProcessNumber === 3) {
        whichGuideIcons = <GuideIconsThird number={props.number - 1} />;
    }

    return (
        <div className={entryPoint__wrapper}>
            <div className="entryPoint">
                <div className={classValue} onClick={() => props.changePhase(props.number - 1)} onKeyPress={() => props.changePhase(props.number - 1)} tabIndex="0">
                    <h5 className={classValueNumber}>
                        {props.number}
                    </h5>
                    <div className="entryPoint__process__image">
                        {whichGuideIcons}
                    </div>
                </div>
                <div className="entryPoint__arrow">
                    {showArrow}
                </div>
            </div>
        </div>
    )
}

export default singleEntryPoint
