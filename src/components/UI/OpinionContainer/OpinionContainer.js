import React from 'react'
import './OpinionContainer.scss'
import classNames from 'classnames'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle, faTimesCircle } from '@fortawesome/free-regular-svg-icons'

const opinionContainer = (props) => {

    const opinionClass = classNames({
        'opinionContainer': true,
        'opinionContainer--advantage': props.type === 'advantage',
        'opinionContainer--disAdvantage': props.type === 'disadvantage'
    })

    let sign = null;
    if (props.type === "advantage") {
        sign = faCheckCircle
    } else {
        sign = faTimesCircle
    }

    return (
        <div className={opinionClass}>
            <p className="opinionContainer__text">{props.text}</p>
            <FontAwesomeIcon className="opinionContainer__icon" icon={sign} />
        </div>
    )
}

export default opinionContainer
