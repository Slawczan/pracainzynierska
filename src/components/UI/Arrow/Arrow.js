import React from 'react'
import './Arrow.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronRight, faChevronLeft } from '@fortawesome/free-solid-svg-icons'

const arrow = (props) => {

    let arrow = null

    if (props.side === "right" & props.phase !== props.totalAmountPhase - 1) {

        arrow = (
            <div className="arrow arrow--right"
                onKeyPress={props.nextAnimation}
                onClick={props.nextAnimation}
                tabIndex="0"
            >
                <FontAwesomeIcon className="arrow__icon"
                    icon={faChevronRight}
                />
            </div>
        )
    }

    if (props.side === "left" & props.phase !== 0) {
        arrow = (
            <div className="arrow arrow--left"
                onKeyPress={props.beforeAnimation}
                onClick={props.beforeAnimation}
                tabIndex="0"
            >
                <FontAwesomeIcon className="arrow__icon"
                    icon={faChevronLeft}
                />
            </div>
        )
    }

    return (
        <React.Fragment>
            {arrow}
        </React.Fragment>
    )
}

export default arrow
