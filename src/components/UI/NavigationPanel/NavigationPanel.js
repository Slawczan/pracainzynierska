import React from 'react'
import './NavigationPanel.scss'
import { Row, Col, Button } from 'react-bootstrap'

const navigationPanel = (props) => {
    return (
        <Row className="navigation">
            <Col className="navigation__arrow__controls">
                <Button className="itemInBoxAnimation__controls__button" onClick={props.setStopButton}> Stop </Button>
                <Button className="itemInBoxAnimation__controls__button" onClick={props.setPlayButton}> Play </Button>
                <Button className="itemInBoxAnimation__controls__button" onClick={props.setPauseButton}> Pause </Button>
            </Col>
        </Row>
    )
}

export default navigationPanel
