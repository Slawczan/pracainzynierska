import React from 'react'
import './MessageBox.scss'

import { Transition, animated } from 'react-spring/renderprops'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInfo } from '@fortawesome/free-solid-svg-icons'

const messageBox = (props) => {
    let show = null
    let icon = null

    if (props.phase === props.totalAmountPhase - 1) {
        return null
    }

    if (props.isOpen) {
        show = (
            <div className="messageBox" onClick={props.messageBoxClicked}>
                <h4 className="messageBox__title">{props.display.phase + 1}. {props.display.title}</h4>
                <p className="messageBox__description">{props.display.description}</p>
            </div>
        )
    } else {
        icon = (
            <FontAwesomeIcon className="messageBox__icon" icon={faInfo} />
        )
    }

    return (
        <React.Fragment>
            <button className="messageBox__button" onClick={props.messageBoxClicked}>
                {icon}
            </button>
            <Transition
                config={{ duration: 750 }}
                items={show}
                from={{
                    position: 'fixed',
                    zIndex: 2,
                    bottom: 0,
                    right: 0,
                    transform: 'translate3d(500px,0,0)',
                    opacity: 1
                }}
                enter={{
                    position: 'fixed',
                    bottom: 0,
                    right: 0,
                    zIndex: 2,
                    transform: 'translate3d(0,0,0)',
                    opacity: 1
                }}
                leave={{
                    position: 'fixed',
                    zIndex: 2,
                    bottom: 0,
                    right: 0,
                    transform: 'translate3d(500px,0,0)',
                    opacity: 1
                }}>
                {(show) => show && (props => <animated.div className="InfoAnimation" style={props}>{show}</animated.div>)}
            </Transition>
        </React.Fragment >
    )
}

export default messageBox
