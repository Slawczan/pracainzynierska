import React from 'react'
import './WarningMessage.scss'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExclamation } from '@fortawesome/free-solid-svg-icons'

const warningMessage = (props) => {

    return (
        <div className="warningMessage">
            <h4 className="warningMessage__text">
                {props.children}
            </h4>
            <FontAwesomeIcon className="warningMessage__icon" icon={faExclamation} />
        </div>
    )
}

export default warningMessage
