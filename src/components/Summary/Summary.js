import React from 'react'
import './Summary.scss'

import { Row, Col, Button } from 'react-bootstrap'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFlagCheckered } from '@fortawesome/free-solid-svg-icons'

import ShoppingItem from '../../components/ShoppingItem/ShoppingItem'
import DeliveryItem from '../../components/DeliveryItem/DeliveryItem'

const summary = (props) => {

    let show = props.choosenItems.map((item, index) => {
        return (
            <ShoppingItem key={"itemName" + index} name={item.name} imageUrl={item.imageUrl} md={3} clicked={null} />
        )
    })

    return (
        <div className="summaryContainer">
            <div className="summary">
                <Row className="summary__title">
                    <h1 className="summary__text">Wizualizacja procesu zakończona
                    <FontAwesomeIcon icon={faFlagCheckered} className="summary__icon" />
                    </h1>
                </Row>

                <Row className="summary__itemList justify-content-md-center">
                    <h3 className="summary__text">Dostarczone przedmioty:</h3>
                    {show}
                </Row>

                <Row className="summary__delivery justify-content-md-center">
                    <h3 className="summary__text">Wybrany sposób dostawy:</h3>
                    <DeliveryItem name={props.choosenDeliveryMethod} />
                </Row>

                <Row className="summary__buttons">
                    <Col md={6}>
                        <Button className="summary__buttons__specificInfo" onClick={props.forceInfo}><h4>Zobacz szczegółowe informacje</h4></Button>
                    </Col>
                    <Col md={6}>
                        <Button className="summary__buttons__restart" onClick={props.restartAplication}><h4>Ja chcę jeszcze raz!</h4></Button>
                    </Col>
                </Row>
            </div >
        </div>

    )
}

export default summary
