import React, { Component } from 'react'

import { Transition, animated } from 'react-spring/renderprops'

import ShoppingPanel from './ShoppingPanel/ShoppingPanel'
import ChoicesPanel from '../components/ChoicesPanel/ChoicesPanel'
import FirstProcesChoice from './ProcessChoices/FirstProcesChoice'
import SecondProcesChoice from './ProcessChoices/SecondProcesChoice'
import ThirdProcesChoice from './ProcessChoices/ThirdProcesChoice'
import Backdrop from '../components/UI/Backdrop/Backdrop'
import WelcomeBox from '../components/WelcomeBox/WelcomeBox'

export class App extends Component {

    state = {
        choosenScenario: null,
        choosenItems: null,
        choosenDeliveryMethod: null,
        running: true,
        forceInfo: false,
        phase: 0,
        timeDuration: 1500,
        welcomeBox: true
    }

    constructor(props) {
        super(props)
        setTimeout(() => {
            this.setState({ running: false });
        }, this.state.timeDuration);
    }

    shoppingHandler = (choosenItems, choosenDeliveryMethod) => {
        this.setState({ choosenItems: choosenItems, choosenDeliveryMethod: choosenDeliveryMethod, running: true })
        setTimeout(() => {
            this.setState({ phase: 1, running: false });
        }, this.state.timeDuration);
    }

    scenarioHandler = (index, forceInfo = false) => {
        this.setState({ choosenScenario: index, forceInfo: forceInfo, running: true })
        setTimeout(() => {
            this.setState({ phase: 2, running: false });
        }, this.state.timeDuration);
    }

    modalHandler = () => {
        this.setState({ welcomeBox: false })
    }

    restartAplication = () => {
        this.setState({
            choosenScenario: null,
            choosenItems: null,
            choosenDeliveryMethod: null,
            running: true,
            phase: 0,
            welcomeBox: true
        })
        setTimeout(() => {
            this.setState({ running: false });
        }, this.state.timeDuration);
    }

    render() {
        let displayModal = null;
        if (this.state.welcomeBox === true) {
            displayModal = (
                <React.Fragment>
                    <Backdrop deleteModal={this.modalHandler} />
                    <WelcomeBox deleteModal={this.modalHandler} />
                </React.Fragment>
            )
        }

        let show = null;
        if (this.state.phase === 0 & this.state.running === false) {
            show = (
                <React.Fragment>
                    <ShoppingPanel
                        errorShow={(message) => this.errorShowHandler(message)}
                        shoppingHandler={(choosenItems, choosenDeliveryMethod) => this.shoppingHandler(choosenItems, choosenDeliveryMethod)}
                    />
                    {displayModal}
                </React.Fragment>

            )
        }
        if (this.state.phase === 1 & this.state.running === false) {
            show = (
                <ChoicesPanel choice={(index, forceInfo) => this.scenarioHandler(index, forceInfo)} />
            )
        }
        if (this.state.phase === 2 & this.state.running === false & this.state.choosenScenario === "0") {
            show = (
                <FirstProcesChoice
                    choosenItems={this.state.choosenItems}
                    choosenDeliveryMethod={this.state.choosenDeliveryMethod}
                    restartAplication={this.restartAplication}
                    forceInfo={this.state.forceInfo}
                    timeDuration={this.state.timeDuration}
                />
            )
        }
        if (this.state.phase === 2 & this.state.running === false & this.state.choosenScenario === "1") {
            show = (
                <SecondProcesChoice choosenItems={this.state.choosenItems}
                    choosenDeliveryMethod={this.state.choosenDeliveryMethod}
                    restartAplication={this.restartAplication}
                    forceInfo={this.state.forceInfo}
                    timeDuration={this.state.timeDuration}
                />
            )
        }
        if (this.state.phase === 2 & this.state.running === false & this.state.choosenScenario === "2") {
            show = (
                <ThirdProcesChoice choosenItems={this.state.choosenItems}
                    choosenDeliveryMethod={this.state.choosenDeliveryMethod}
                    restartAplication={this.restartAplication}
                    forceInfo={this.state.forceInfo}
                    timeDuration={this.state.timeDuration}
                />
            )
        }

        return (
            <React.Fragment>
                <Transition
                    config={{ duration: 400 }}
                    items={displayModal}
                    from={{
                        opacity: 0,
                        zIndex: 200
                    }}
                    enter={{
                        opacity: 1,
                        zIndex: 200
                    }}
                    leave={{
                        opacity: 0,
                        zIndex: 200
                    }}>
                    {(displayModal) => displayModal && (props => <animated.div className="welcome" style={props}>{displayModal}</animated.div>)}
                </Transition>
                <Transition
                    config={{ duration: this.state.timeDuration }}
                    items={show}
                    from={{ opacity: 0 }}
                    enter={{ opacity: 1 }}
                    leave={{ opacity: 0 }}>
                    {(show) => show && (props => <animated.div className="app" style={props}>{show}</animated.div>)}
                </Transition>
            </React.Fragment >

        )
    }
}

export default App

