import React, { Component } from 'react'
import './ShoppingPanel.scss'

import { Container, Row, Button, Form, Col } from 'react-bootstrap'

import { Transition, animated } from 'react-spring/renderprops'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'

import ShoppingItem from '../../components/ShoppingItem/ShoppingItem'
import WarningMessage from '../../components/UI/WarningMessage/WarningMessage'
import DeliveryItem from '../../components/DeliveryItem/DeliveryItem'

export class ShoppingPanel extends Component {

    state = {
        shopingItems: [
            {
                name: 'Książka',
                selected: false
            },
            {
                name: 'Płyta',
                selected: false
            },
            {
                name: 'Gazeta',
                selected: false
            },
            {
                name: 'Gra planszowa',
                selected: false
            },
        ],
        deliveryMethod: [
            'Poczta',
            'Paczkomat',
            'Kurier(DHL)'
        ],
        choosenDeliveryMethod: null,
        warning: false
    }

    inputClickedHandler = (index) => {
        let newState = this.state.shopingItems.slice()
        newState[index].selected = !newState[index].selected
        this.setState({ shopingFirstItems: newState, warning: false })
    }

    // inputChangedHandler = (event) => {
    //     this.setState({ choosenDeliveryMethod: event.target.value })
    // }

    deliveryMethodHandler = (index) => {
        this.setState({ choosenDeliveryMethod: this.state.deliveryMethod[index], warning: false })
    }

    handleConfirm = () => {
        let selectedItems = [];
        this.state.shopingItems.forEach(element => {
            if (element.selected) {
                selectedItems.push({
                    name: element.name
                })
            }
        })
        if (selectedItems.length === 0) {
            this.setState({ warning: true })
            return null
        }
        if (this.state.choosenDeliveryMethod === null) {
            this.setState({ warning: true })
            return null
        }
        this.props.shoppingHandler(selectedItems, this.state.choosenDeliveryMethod)
    }

    render() {
        let shoppingItems = this.state.shopingItems.map((item, index) => {
            return (
                <ShoppingItem key={index} name={item.name} selected={item.selected} md={3} clicked={() => this.inputClickedHandler(index)} />
            )
        })

        let warn = null
        if (this.state.warning) {
            if (this.state.choosenDeliveryMethod !== null) {
                warn = (
                    <WarningMessage>
                        Zaznacz przynajmniej jedną rzecz
                    </WarningMessage>
                )
            } else {
                warn = (
                    <WarningMessage>
                        Zaznacz sposób dostawy
                    </WarningMessage>
                )
            }

        }

        let deliveryItems = this.state.deliveryMethod.map((item, index) => {
            let selected = false
            if (this.state.choosenDeliveryMethod === item) {
                selected = true
            }
            return (
                <DeliveryItem key={"DeliveryItem" + index} name={item} selected={selected} clicked={() => this.deliveryMethodHandler(index)} />
            )
        })

        return (

            <Container className="shoppingPanelContainer">

                <Transition
                    config={{ duration: 400 }}
                    items={warn}
                    from={{
                        transform: 'translate3d(0,-50px,0)'
                    }}
                    enter={{
                        transform: 'translate3d(0,0,0)'
                    }}
                    leave={{
                        transform: 'translate3d(0,-50px,0)'
                    }}>
                    {warn => warn && (props => <animated.div style={props}>{warn}</animated.div>)}
                </Transition>

                <div className="shoppingPanel">
                    <Row>
                        <h2 className="shoppingPanel__title">
                            Sklep internetowy
                            <FontAwesomeIcon className="shoppingPanel__title__icon" icon={faShoppingCart} />
                        </h2>
                    </Row>

                    <div className="shoppingPanel__shop">
                        <Row>
                            <h3 className="shoppingPanel__subtitle">Wybierz przedmioty do zakupienia</h3>
                        </Row>

                        <Row className="shoppingPanel__buyItems">
                            {shoppingItems}
                        </Row>

                        {/* <Form.Control as="select"
                        className='shoppingPanel__select'
                        value={this.state.value}
                        onChange={(event) => this.inputChangedHandler(event)}
                    >
                        {this.state.deliveryMethod.map(option => (
                            <option
                                key={option}
                                value={option}>
                                {option}
                            </option>
                        ))}
                    </Form.Control> */}

                        <Row >
                            <h3 className="shoppingPanel__subtitle">Wybierz sposób dostawy</h3>
                        </Row>

                        <Row className="shoppingPanel__delivery">
                            {deliveryItems}
                        </Row>
                    </div>


                    <Button className="shoppingPanel__button" onClick={this.handleConfirm}>Potwierdź</Button>
                </div>
            </Container >
        )
    }
}

export default ShoppingPanel
