import React, { Component } from 'react'
import './FirstProcesChoice.scss'

import { Container } from 'react-bootstrap'

import { Transition, animated } from 'react-spring/renderprops'

import WarehouseCheck from '../../components/WarehouseCheck/WarehouseCheck'
import RecoveringMessageAnimation from '../../components/FirstChoice/RecoveringMessageAnimation/RecoveringMessageAnimation'
import WarehouseSearch from '../../components/FirstChoice/WarehouseSearch/WarehouseSearch'
import Scaner from '../../components/FirstChoice/Scaner/Scaner'
import ItemInBox from '../../components/FirstChoice/ItemsInBox/ItemInBox'
import MovingToPacking from '../../components/FirstChoice/MovingToPacking/MovingToPacking'
import PackingItems from '../../components/FirstChoice/PackingItems/PackingItems'
import DeliveryProcess from '../../components/DeliveryProcess/DeliveryProcess'
import CheckingStickingPhase from '../../components/FirstChoice/CheckingStickingPhase/CheckingStickingPhase'
import MovingToWarehouse from '../../components/FirstChoice/MovingToWarehouse/MovingToWarehouse'
import SpecificInfo from '../../components/FirstChoice/SpecificInfo/SpecificInfo'
import Payment from '../../components/SecondChoice/Payment'

import Summary from '../../components/Summary/Summary'
import Arrow from '../../components/UI/Arrow/Arrow'
import MessageBox from '../../components/UI/MessageBox/MessageBox'
import GuideBox from '../../components/UI/GuideBox/GuideBox'

export class ThirdProcesChoice extends Component {

    state = {
        phase: 0,
        totalAmountPhase: 13,
        choiceProcessNumber: 3,
        messageBoxIsOpen: true,
        guideBoxIsOpen: true,
        running: false,
        forceInfo: this.props.forceInfo,
        title: "Inne podejście cross-docking",
        processDescription: "Wizualizacja która została przedstawiona w sposób symboliczny przedstawia proces dostarczania zamówienia wybranego przez klienta w pierwszym etapie symulacji "
            + "do jego rąk własnych. W scenariusz który jest prezentowany magazyn pokazany jest w ujęciu dystrybucyjnym. W tym konkrentym przypadku najważniejszą zasada "
            + "działania tego rodzaju magazynu jest przetrzymywanie jak najmniejszej ilości przedmiotów. Produkty które wybrał klient są zamawiane do magazynu dopiero "
            + "w momencie potwierdzenia zamówienia a cały proces rozpoczyna się po ich dostarczeniu.",
        advantages: [
            "Niski kosz utrzymania magazynu ze względu na wielkość",
            "Mały kosz związany z długością przetrzymywania towarów w magazynie ",
            "Zwiększenie szybkości odnajdywania towarów z powodu małej ilości rzeczy w magazynie"
        ],
        disadvantages: [
            "Dłuższy czas dostarczenia zamówienia klientowi spowodowany oczekiwaniem na dostarczenie towaru przez sprzedawców",
            "Większe zamieszenie oraz koszt w przypadku zwrócenia towaru przez klienta",
        ],
        displayText: [
            {
                phase: 0,
                title: "Zamówienie przedmiotów dla klienta",
                description: "Pracownik po otrzymaniu informacji z systemu zamawia potrzebne przedmioty dla klienta. Sprzedawca przygotowuje odpowiedni towar do przesyłki.",
                place: "Główny magazyn",
                time: 30 * 60,
            },
            {
                phase: 1,
                title: "Dostawa przedmiotów do magazynu",
                description: "Przedmioty zostają przetransportowane od sprzedawcy do magazynu poprzez wybrany ustalony sposób dostawy, następnie rozpakowane i wprowadzone "
                    + "do bazy danych. ",
                place: "Magazyn dostawczy klienta",
                time: 60 * 60 * 24,
            },
            {
                phase: 2,
                title: "Nadzorowanie systemu",
                description: "System nadzoruje przebieg dostarczenia towarów do magazynu. Gdy wszystkie są dostępne rozpoczyna proces dostarczenia paczki w magazynie.",
                place: "Główny magazyn",
                time: 5,
            },
            {
                phase: 3,
                title: "Informacja dla pracownika",
                description: "System zlecający informuje pracownia o dotarciu wszystkich potrzebny przedmiotów do magazynu. Zlecenie zostaje przyjęte przez pracownika. "
                    + "Szczegółowe informacje zostają wyświetlone na monitorze jak również zapisane w urządzeniu skanującym.",
                place: "Główny magazyn",
                time: 5 * 60,
            },
            {
                phase: 4,
                title: "Poszukiwanie przedmiotu w magazynie",
                description: "Pracownik wyrusza na magazyn w poszukiwaniu zleconych przedmiotów. Zabiera ze sobą wózek z koszykiem oraz skaner służący do komunikacji z system zarządzającym procesem.",
                place: "Główny magazyn"
            },
            {
                phase: 5,
                title: "Skanowanie znalezionego przedmiotu",
                description: "Po znalezieniu przedmiotu w magazynie skanuje się go skanerem aby potwierdzić czy dany przedmiot jest identyczny z zamówionym. Dodatkowo  urządzenie posiada wyświetlacz na który jest wyświetlona trasa do kolejnego przedmiotu.",
                place: "Główny magazyn"
            },
            {
                phase: 6,
                title: "Znalezienie wszystkiech zamówień",
                description: "Pracownik wykonuję odpowiednie działanie na skanerze która potwierdza systemowi nadzorującemu że wszystkie przedmioty zostały odnalezione w magazynie. Zamówienie które znajduję się w koszyku jest gotowe do pakowania.",
                place: "Główny magazyn"
            },
            {
                phase: 7,
                title: "Przesłanie do pakowania",
                description: "Pracownik informuje system zarządzający całym procesem o przesłaniu znalezionych przedmiotów do pakowania poprzez zeskanowanie kodu kreskowego koszyka. Zamówienie jest transportowane do placówki pakującej.",
                place: "Główny magazyn",
                time: 60
            },
            {
                phase: 8,
                title: "Pakowanie",
                description: "Pod nadzorem systemu zarządzajego pracownik pakuję przedmioty z koszyka do przygotowanej wcześniej paczki w odpowiedniej kolejności.",
                place: "Placówka pakująca",
                time: 3 * 60
            },
            {
                phase: 9,
                title: "Sprawdzenie poprawności zamówienia",
                description: "System nadzorujący pokazuję informację o zamówieniu na ekranie monitora. Pracownik sprawdza skanerem poprawność zamówienia, jakość produktów oraz sprawdza wagę przedmiotów poprzez porównanie z wagą wyliczoną przez system zarządzania.",
                place: "Placówka pakująca",
                time: 4 * 60
            },
            {
                phase: 10,
                title: "Przesłanie do magazynu",
                description: "Pracownik informuje system zarządzający całym procesem o przesłaniu kompletnej paczki do magazynu dostawczego poprzez zeskanowanie kodu kreskowego paczki. Zamówienie jest transportowane do magazynu dostawczego.",
                place: "Placówka pakująca",
                time: 60 * 2
            },
            {
                phase: 11,
                title: "Dostarczenie paczki",
                description: "W magazynie przedmioty oczekują na odpowiedni transport który został wybrany przez klienta. Gdy zjawi się odpowiedni dostawca paczka zostaje zeskanowana w celu poinformowania systemu o zabraniu jej z magazynu dostawczego i zabrana w celu dostarczenia.",
                place: "Magazyn dostawczy",
                time: 60 * 60 * 72
            }
        ],
    }

    nextAnimation = () => {
        console.log(this.props.timeDuration)
        let phase = this.state.phase
        phase++
        this.setState({ messageBoxIsOpen: false, guideBoxIsOpen: false, running: true })
        setTimeout(() => {
            this.setState({ messageBoxIsOpen: true, phase: phase, running: false })
        }, this.props.timeDuration)
    }

    beforeAnimation = () => {
        let phase = this.state.phase
        phase--
        this.setState({ messageBoxIsOpen: false, guideBoxIsOpen: false, running: true })
        setTimeout(() => {
            this.setState({ messageBoxIsOpen: true, phase: phase, running: false })
        }, this.props.timeDuration)
    }

    messageBoxHandler = () => {
        let messageBoxIsOpen = this.state.messageBoxIsOpen
        messageBoxIsOpen = !messageBoxIsOpen
        this.setState({ messageBoxIsOpen: messageBoxIsOpen })
    }

    guideBoxHandler = () => {
        let guideBoxIsOpen = this.state.guideBoxIsOpen
        guideBoxIsOpen = !guideBoxIsOpen
        this.setState({ guideBoxIsOpen: guideBoxIsOpen })
    }

    guideBoxChangePhaseHandler = (phase) => {
        this.setState({ messageBoxIsOpen: false, forceInfo: false, guideBoxIsOpen: false, running: true })
        setTimeout(() => {
            this.setState({ messageBoxIsOpen: true, phase: phase, running: false })
        }, this.props.timeDuration)
    }

    forceInfoComponent = () => {
        let forceInfo = this.state.forceInfo
        forceInfo = !forceInfo
        this.setState({ running: true, messageBoxIsOpen: false, guideBoxIsOpen: false })
        setTimeout(() => {
            this.setState({ forceInfo: forceInfo, running: false })
        }, this.props.timeDuration)
    }

    render() {
        let show = null;
        let arrowLeft = null;
        let arrowRight = null;
        let guideMessage = null;
        let infoMessage = null;

        if (this.state.forceInfo === true && this.state.running === false) {
            show = <SpecificInfo
                {...this.props}
                forceInfo={this.forceInfoComponent}
                phase={this.state.phase}
                totalAmountPhase={this.state.totalAmountPhase}
                changePhase={(index) => this.guideBoxChangePhaseHandler(index)}
                title={this.state.title}
                processDescription={this.state.processDescription}
                processStages={this.state.displayText}
                choosenItems={this.props.choosenItems}
                advantages={this.state.advantages}
                disadvantages={this.state.disadvantages}
                choiceProcessNumber={this.state.choiceProcessNumber}
                findTime={2 * 60}
            />
        } else {

            if (this.state.phase === 0 && this.state.running === false) {
                show = <Payment />
            }

            if (this.state.phase === 1 && this.state.running === false) {
                show = <DeliveryProcess
                    {...this.props}
                    choosenDeliveryMethod={"Kurier(DHL)"}
                />
            }

            if (this.state.phase === 2 && this.state.running === false) {
                show = <WarehouseCheck
                    {...this.props}
                />
            }

            if (this.state.phase === 3 && this.state.running === false) {
                show = <RecoveringMessageAnimation
                    {...this.props}
                />
            }

            if (this.state.phase === 4 && this.state.running === false) {
                show = <WarehouseSearch />
            }

            if (this.state.phase === 5 && this.state.running === false) {
                show = <Scaner />
            }

            if (this.state.phase === 6 && this.state.running === false) {
                show = <ItemInBox />
            }

            if (this.state.phase === 7 && this.state.running === false) {
                show = <MovingToPacking />
            }

            if (this.state.phase === 8 && this.state.running === false) {
                show = <PackingItems />
            }

            if (this.state.phase === 9 && this.state.running === false) {
                show = <CheckingStickingPhase />
            }

            if (this.state.phase === 10 && this.state.running === false) {
                show = <MovingToWarehouse />
            }

            if (this.state.phase === 11 && this.state.running === false) {
                show = <DeliveryProcess
                    {...this.props}
                />
            }

            if (this.state.phase === this.state.totalAmountPhase - 1 && this.state.running === false) {
                show = <Summary
                    {...this.props}
                    forceInfo={this.forceInfoComponent}
                />
            }

            arrowLeft = (
                <Arrow side={"left"}
                    nextAnimation={this.nextAnimation}
                    beforeAnimation={this.beforeAnimation}
                    phase={this.state.phase}
                    totalAmountPhase={this.state.totalAmountPhase}
                />
            )
            guideMessage = (
                <GuideBox
                    display={this.state.displayText[this.state.phase]}
                    guideBoxClicked={this.guideBoxHandler}
                    isOpen={this.state.guideBoxIsOpen}
                    phase={this.state.phase}
                    totalAmountPhase={this.state.totalAmountPhase}
                    changePhase={(index) => this.guideBoxChangePhaseHandler(index)}
                    forceInfo={this.forceInfoComponent}
                    choiceProcessNumber={this.state.choiceProcessNumber}
                />
            )

            arrowRight = (
                <Arrow side={"right"}
                    nextAnimation={this.nextAnimation}
                    beforeAnimation={this.beforeAnimation}
                    phase={this.state.phase}
                    totalAmountPhase={this.state.totalAmountPhase}
                />
            )

            infoMessage = (
                <MessageBox
                    display={this.state.displayText[this.state.phase]}
                    messageBoxClicked={this.messageBoxHandler}
                    isOpen={this.state.messageBoxIsOpen}
                    phase={this.state.phase}
                    totalAmountPhase={this.state.totalAmountPhase}
                />
            )
        }

        return (
            <React.Fragment>
                {arrowLeft}

                {guideMessage}

                {arrowRight}

                {infoMessage}

                <Container className="firstProcessContainer">
                    <Transition
                        config={{ duration: this.props.timeDuration }}
                        items={show}
                        from={{ opacity: 0 }}
                        enter={{ opacity: 1 }}
                        leave={{ opacity: 0 }}>
                        {(show) => show && (props => <animated.div className="app" style={props}>{show}</animated.div>)}
                    </Transition>
                </Container>
            </React.Fragment>
        )
    }
}

export default ThirdProcesChoice

