import React, { Component } from 'react'
import './FirstProcesChoice.scss'

import { Container } from 'react-bootstrap'

import { Transition, animated } from 'react-spring/renderprops'

import WarehouseCheck from '../../components/WarehouseCheck/WarehouseCheck'
import RecoveringMessageAnimation from '../../components/FirstChoice/RecoveringMessageAnimation/RecoveringMessageAnimation'
import WarehouseSearch from '../../components/FirstChoice/WarehouseSearch/WarehouseSearch'
import Scaner from '../../components/FirstChoice/Scaner/Scaner'
import ItemInBox from '../../components/FirstChoice/ItemsInBox/ItemInBox'
import MovingToPacking from '../../components/FirstChoice/MovingToPacking/MovingToPacking'
import PackingItems from '../../components/FirstChoice/PackingItems/PackingItems'
import DeliveryProcess from '../../components/DeliveryProcess/DeliveryProcess'
import CheckingStickingPhase from '../../components/FirstChoice/CheckingStickingPhase/CheckingStickingPhase'
import MovingToWarehouse from '../../components/FirstChoice/MovingToWarehouse/MovingToWarehouse'
import SpecificInfo from '../../components/FirstChoice/SpecificInfo/SpecificInfo'
import Payment from '../../components/SecondChoice/Payment'

import Summary from '../../components/Summary/Summary'
import Arrow from '../../components/UI/Arrow/Arrow'
import MessageBox from '../../components/UI/MessageBox/MessageBox'
import GuideBox from '../../components/UI/GuideBox/GuideBox'

export class SecondProcesChoice extends Component {

    state = {
        phase: 0,
        totalAmountPhase: 14,
        choiceProcessNumber: 2,
        messageBoxIsOpen: true,
        guideBoxIsOpen: true,
        running: false,
        forceInfo: this.props.forceInfo,
        title: "Brakujący przedmiot",
        processDescription: "Wizualizacja która została przedstawiona w sposób symboliczny przedstawia proces dostarczania zamówienia wybranego przez klienta w pierwszym etapie "
            + "symulacji do jego rąk własnych. W scenariusz który jest prezentowany magazyn jest ukazany w ujęciu dystrybucyjnym.W tym konkretym przypadku prezentuję się magazyn w podejściu klasycznym. "
            + "W wybranej symulacji prezentowana jest przeszkoda jaką jest brak konkretnego przedmiotu bądź przedmiotów w magazynie. Gdy taka sytuacja następuję system zarządzający "
            + "procesem podejmuje odpowiednie kroki naprawcze. Problem który jest prezentowany skutkuje wydłużeniem procesu. Cały proces podzielony jest na kilka ważnych etapów "
            + "które łączą się w jedną całość. Zaprezentowana symulacja rozpoczyna się natychmiasta po potwierdzenia zamówienia od klienta która w naszym przypadku było przejście"
            + "pierwszego etapu aplikacji. Należy zaznaczyć że poszczególne wybory w pierwszym etapie skutkują różnymi animacjami pojawiającymi się na ekranie. ",
        advantages: [
            "Duża pojemność magazynu wiąże się z dużą ilością miejsca na przedmioty",
            "Łatwy zwrot przedmiotu do magazynu w razie pomyłki w trakcie procesu",
            "Łatwy zwrot towaru przez klienta"
        ],
        disadvantages: [
            "Duży magazyn wiąże się z dużymi kosztami",
            "Czym większe zamówienie tym dłuższa droga pracownika po magazynie w celu znalezienie poszczególnych przedmiotów",
        ],
        displayText: [
            {
                phase: 0,
                title: "Sprawdzenie w bazie danych",
                description: "System po otrzymaniu potwierdzenie zamówienia rozpoczyna cały proces. "
                    + "Zlecenie zostaje wyszukane w bazie danych."
                    + " System informuje pracownika o nie odnalezieniu poszczególnych przedmiotów, a klienta o dłuższym czasie oczekiwania.",
                place: "Główny magazyn",
                time: 5,
            },
            {
                phase: 1,
                title: "Dokupienie brakujących przedmiotów",
                description: "Pracownik po otrzymaniu informacji z systemu zakupuje brakujące przedmioty. Sprzedawca przetwarza to w swoim systemie i informuję magazyn o wysłaniu paczki. Proces w magazynie zostaje wstrzymany do czasu dostarczenia produktu.",
                place: "Główny magazyn",
                time: 30 * 60,
            },
            {
                phase: 2,
                title: "Dostarczenie przedmiotu do magazynu",
                description: "Klient który sprzedaje dany przedmiotu dostarcza poprzez dostawcę produkt do magazynu. Tam po odebraniu zostaje dodany do bazy danych magazynu a proces dostarczenia zamówienia do klienta zostaje wznowiony.",
                place: "Magazyn dostawczy klienta",
                time: 60 * 60 * 24,
            },
            {
                phase: 3,
                title: "Ponowne sprawdzenie w bazie danych",
                description: "Zlecone przedmioty zostają ponownie sprawdzone w bazie danych magazynu. Patrząc na poprzednie kroki procesu przedmioty powinny się w nim znajdować. ",
                place: "Główny magazyn",
                time: 5,
            },
            {
                phase: 4,
                title: "Informacja dla pracownika",
                description: "Wszystie przedmioty znajdują się w bazie danych. System informuje pracownika o zadaniu znalezienia zamówienia. "
                    + "Zlecenie zostaje przyjęte przez pracownika. Informacje zostają wyświetlone na monitorze jak również zapisane w urządzeniu skanującym.",
                place: "Główny magazyn",
                time: 5 * 60,
            },
            {
                phase: 5,
                title: "Poszukiwanie przedmiotu w magazynie",
                description: "Pracownik wyrusza na magazyn w poszukiwaniu zleconych przedmiotów. Zabiera ze sobą wózek z koszykiem oraz skaner służący do komunikacji z system zarządzającym procesem",
                place: "Główny magazyn"
            },
            {
                phase: 6,
                title: "Skanowanie znalezionego przedmiotu",
                description: "Po znalezieniu przedmiotu w magazynie skanuję się go skanerem aby potwierdzić czy dany przedmiot jest identyczny z zamówionym. Dodatkowo  urządzenie posiada wyświetlacz na który jest wyświetlona trasa do kolejnego przedmiotu.",
                place: "Główny magazyn"
            },
            {
                phase: 7,
                title: "Znalezienie wszystkiech zamówień",
                description: "Pracownik wykonuję odpowiednie działanie na skanerze która potwierdza systemowi nadzorującemu że wszystkie przedmioty zostały odnalezione w magazynie. Zamówienie które znajduję się w koszyku jest gotowe do pakowania.",
                place: "Główny magazyn"
            },
            {
                phase: 8,
                title: "Przesłanie do pakowania",
                description: "Pracownik informuje system zarządzający całym procesem o przesłaniu znalezionych przedmiotów do pakowania poprzez zeskanowanie kodu kreskowego koszyka. Zamówienie jest transportowane do placówki pakującej.",
                place: "Główny magazyn",
                time: 60
            },
            {
                phase: 9,
                title: "Pakowanie",
                description: "Pod nadzorem systemu zarządzajego pracownik pakuje przedmioty z koszyka do przygotowanej wcześniej paczki w odpowiedniej kolejności.",
                place: "Placówka pakująca",
                time: 3 * 60
            },
            {
                phase: 10,
                title: "Sprawdzenie poprawności zamówienia",
                description: "System nadzorujący pokazuję informację o zamówieniu na ekranie monitora. Pracownik sprawdza skanerem poprawność zamówienia, jakość produktów oraz sprawdza wagę przedmiotów poprzez porównanie z wagą wyliczoną przez system zarządzania.",
                place: "Placówka pakująca",
                time: 4 * 60
            },
            {
                phase: 11,
                title: "Przesłanie do magazynu",
                description: "Pracownik informuje system zarządzający całym procesem o przesłaniu kompletnej paczki do magazynu dostawczego poprzez zeskanowanie kodu kreskowego paczki. Zamówienie jest transportowane do magazynu dostawczego.",
                place: "Placówka pakująca",
                time: 60 * 2
            },
            {
                phase: 12,
                title: "Dostarczenie paczki",
                description: "W magazynie przedmioty oczekują na odpowiedni transport który został wybrany przez klienta. Gdy zjawi się odpowiedni dostawca paczka zostaje zeskanowana w celu poinformowania systemu o zabraniu jej z magazynu dostawczego i zabrana w celu dostarczenia.",
                place: "Magazyn dostawczy",
                time: 60 * 60 * 72
            }
        ],
    }

    nextAnimation = () => {
        console.log(this.props.timeDuration)
        let phase = this.state.phase
        phase++
        this.setState({ messageBoxIsOpen: false, guideBoxIsOpen: false, running: true })
        setTimeout(() => {
            this.setState({ messageBoxIsOpen: true, phase: phase, running: false })
        }, this.props.timeDuration)
    }

    beforeAnimation = () => {
        let phase = this.state.phase
        phase--
        this.setState({ messageBoxIsOpen: false, guideBoxIsOpen: false, running: true })
        setTimeout(() => {
            this.setState({ messageBoxIsOpen: true, phase: phase, running: false })
        }, this.props.timeDuration)
    }

    messageBoxHandler = () => {
        let messageBoxIsOpen = this.state.messageBoxIsOpen
        messageBoxIsOpen = !messageBoxIsOpen
        this.setState({ messageBoxIsOpen: messageBoxIsOpen })
    }

    guideBoxHandler = () => {
        let guideBoxIsOpen = this.state.guideBoxIsOpen
        guideBoxIsOpen = !guideBoxIsOpen
        this.setState({ guideBoxIsOpen: guideBoxIsOpen })
    }

    guideBoxChangePhaseHandler = (phase) => {
        this.setState({ messageBoxIsOpen: false, forceInfo: false, guideBoxIsOpen: false, running: true })
        setTimeout(() => {
            this.setState({ messageBoxIsOpen: true, phase: phase, running: false })
        }, this.props.timeDuration)
    }

    forceInfoComponent = () => {
        let forceInfo = this.state.forceInfo
        forceInfo = !forceInfo
        this.setState({ running: true, messageBoxIsOpen: false, guideBoxIsOpen: false })
        setTimeout(() => {
            this.setState({ forceInfo: forceInfo, running: false })
        }, this.props.timeDuration)
    }

    render() {
        let show = null;
        let arrowLeft = null;
        let arrowRight = null;
        let guideMessage = null;
        let infoMessage = null;

        if (this.state.forceInfo === true && this.state.running === false) {
            show = <SpecificInfo
                {...this.props}
                forceInfo={this.forceInfoComponent}
                phase={this.state.phase}
                totalAmountPhase={this.state.totalAmountPhase}
                changePhase={(index) => this.guideBoxChangePhaseHandler(index)}
                title={this.state.title}
                processDescription={this.state.processDescription}
                processStages={this.state.displayText}
                choosenItems={this.props.choosenItems}
                advantages={this.state.advantages}
                disadvantages={this.state.disadvantages}
                choiceProcessNumber={this.state.choiceProcessNumber}
                findTime={7 * 60}
            />
        } else {
            if (this.state.phase === 0 && this.state.running === false) {
                show = <WarehouseCheck
                    {...this.props}
                    isNotAvailable={true}
                />
            }

            if (this.state.phase === 1 && this.state.running === false) {
                show = <Payment />
            }

            if (this.state.phase === 2 && this.state.running === false) {
                show = <DeliveryProcess
                    {...this.props}
                    choosenDeliveryMethod={"Kurier(DHL)"}
                />
            }

            if (this.state.phase === 3 && this.state.running === false) {
                show = <WarehouseCheck
                    {...this.props}
                />
            }

            if (this.state.phase === 4 && this.state.running === false) {
                show = <RecoveringMessageAnimation
                    {...this.props}
                />
            }

            if (this.state.phase === 5 && this.state.running === false) {
                show = <WarehouseSearch />
            }

            if (this.state.phase === 6 && this.state.running === false) {
                show = <Scaner />
            }

            if (this.state.phase === 7 && this.state.running === false) {
                show = <ItemInBox />
            }

            if (this.state.phase === 8 && this.state.running === false) {
                show = <MovingToPacking />
            }

            if (this.state.phase === 9 && this.state.running === false) {
                show = <PackingItems />
            }

            if (this.state.phase === 10 && this.state.running === false) {
                show = <CheckingStickingPhase />
            }

            if (this.state.phase === 11 && this.state.running === false) {
                show = <MovingToWarehouse />
            }

            if (this.state.phase === 12 && this.state.running === false) {
                show = <DeliveryProcess
                    {...this.props}
                />
            }

            if (this.state.phase === this.state.totalAmountPhase - 1 && this.state.running === false) {
                show = <Summary
                    {...this.props}
                    forceInfo={this.forceInfoComponent}
                />
            }

            arrowLeft = (
                <Arrow side={"left"}
                    nextAnimation={this.nextAnimation}
                    beforeAnimation={this.beforeAnimation}
                    phase={this.state.phase}
                    totalAmountPhase={this.state.totalAmountPhase}
                />
            )
            guideMessage = (
                <GuideBox
                    display={this.state.displayText[this.state.phase]}
                    guideBoxClicked={this.guideBoxHandler}
                    isOpen={this.state.guideBoxIsOpen}
                    phase={this.state.phase}
                    totalAmountPhase={this.state.totalAmountPhase}
                    changePhase={(index) => this.guideBoxChangePhaseHandler(index)}
                    forceInfo={this.forceInfoComponent}
                    choiceProcessNumber={this.state.choiceProcessNumber}
                />
            )

            arrowRight = (
                <Arrow side={"right"}
                    nextAnimation={this.nextAnimation}
                    beforeAnimation={this.beforeAnimation}
                    phase={this.state.phase}
                    totalAmountPhase={this.state.totalAmountPhase}
                />
            )

            infoMessage = (
                <MessageBox
                    display={this.state.displayText[this.state.phase]}
                    messageBoxClicked={this.messageBoxHandler}
                    isOpen={this.state.messageBoxIsOpen}
                    phase={this.state.phase}
                    totalAmountPhase={this.state.totalAmountPhase}
                />
            )
        }

        return (
            <React.Fragment>
                {arrowLeft}

                {guideMessage}

                {arrowRight}

                {infoMessage}

                <Container className="firstProcessContainer">
                    <Transition
                        config={{ duration: this.props.timeDuration }}
                        items={show}
                        from={{ opacity: 0 }}
                        enter={{ opacity: 1 }}
                        leave={{ opacity: 0 }}>
                        {(show) => show && (props => <animated.div className="app" style={props}>{show}</animated.div>)}
                    </Transition>
                </Container>
            </React.Fragment>
        )
    }
}

export default SecondProcesChoice
